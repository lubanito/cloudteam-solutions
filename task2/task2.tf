##
## Terraform configuration
##


terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
    local = {
      source  = "hashicorp/local"
      version = "~> 2.0"
    }
    http = {
      source  = "hashicorp/http"
      version = "~> 2.0"
    }
    null = {
      source  = "hashicorp/null"
      version = "~> 3.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "~> 3.0"
    }
  }
}


##
## Variables
##


# Default SSH public key file path
variable "default_ssh_public_key_path" {
  type    = string
  default = "~/.ssh/id_rsa.pub"
}

# Default SSH private key file path
variable "default_ssh_private_key_path" {
  type    = string
  default = ""
}

# Additional public IPv4 CIDR blocks allowed SSH access to jump host in DMZ
variable "extra_public_cidr_blocks_allowed_ssh_to_jmp" {
  type    = list(string)
  default = []
}

# Ansible playbook file path
# (playbook to be run, if available, on every Terraform apply
# once instances are created)
variable "ansible_playbook" {
  type    = string
  default = "task2.yml"
}

# Ansible vars file path for asserting Internet access state
variable "ansible_vars_assert_internet_access_state" {
  type    = string
  default = "vars/assert_internet_access_state.yml"
}

# Ansible `all` group vars file path
variable "ansible_group_vars_all" {
  type    = string
  default = "group_vars/all/main.yml"
}

# Ansible inventory (hosts) generated file path
variable "ansible_inventory" {
  type    = string
  default = "hosts"
}

# OpenSSH client configuration generated file path
variable "ssh_config" {
  type    = string
  default = "ssh/config"
}

# Region
variable "region" {
  type    = string
  default = "us-east-1"
}

# Instance type
variable "instance_type" {
  type    = string
  default = "t2.micro" # free tier eligible, x86_64
}

# Name of default user account in default AMI
variable "default_ami_user" {
  type    = string
  default = "ec2-user"
}

# Public Internet HTTP service URL returning client's IPv4 address
# (outbound public address) as string
variable "outbound_public_ip_service_url" {
  type    = string
  default = "https://api.ipify.org/"
}


##
## Provider configuration
##


provider "aws" {
  region = var.region
}


##
## Data sources
##


# Amazon Machine Image:
# Latest non-beta RHEL 7 x86_64
data "aws_ami" "default" {
  most_recent = true

  owners = [309956199498] # Red Hat

  filter {
    name   = "name"
    values = ["RHEL-7.*"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }
}

# SSH public key file
data "local_file" "default_ssh_public_key" {
  filename = pathexpand(var.default_ssh_public_key_path)
}

# Terraform host's outbound public IPv4 address
data "http" "terraform_outbound_public_ip" {
  url = var.outbound_public_ip_service_url
}

# All Availability Zones currently available in the region
data "aws_availability_zones" "available" {
  state = "available"
}


##
## Resources
##


## Common


# SSH key pair / public key
resource "aws_key_pair" "default" {
  public_key = data.local_file.default_ssh_public_key.content
}

# VPC
resource "aws_vpc" "main" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true
}

# VPC's Internet gateway
resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id
}

# Two different available Availability Zones,
# randomly chosen for high-availability deployment
resource "random_shuffle" "az_available" {
  input        = data.aws_availability_zones.available.names
  result_count = 2
}

## Public subnet (DMZ) with Internet access


# Route table (RTb) for public subnets, empty at first
resource "aws_route_table" "sub_pub" {
  vpc_id = aws_vpc.main.id

}

# Post-creation route in RTb for public subnets:
# default route through Internet gateway
resource "aws_route" "default_gw_igw_main" {
  route_table_id         = aws_route_table.sub_pub.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.main.id
}

# VPC's DMZ subnet 0
resource "aws_subnet" "dmz_0" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.0.0/24"
}

# Route table association for DMZ subnet 0
resource "aws_route_table_association" "sub_dmz_0" {
  subnet_id      = aws_subnet.dmz_0.id
  route_table_id = aws_route_table.sub_pub.id
}

# VPC's security group (SG) for public subnet instances having Internet access,
# but accessible only from specified public IPv4 address ranges
resource "aws_security_group" "sub_pub_ingr_spec_egr_any" {
  vpc_id = aws_vpc.main.id
}

# Rule in SG for public subnet instances accessible from Internet,
# allowing ingress to SSH port from Terraform host's outgoung public IP address
resource "aws_security_group_rule" "sub_pub_ingr_ssh_from_terraform" {
  security_group_id = aws_security_group.sub_pub_ingr_spec_egr_any.id
  type              = "ingress"
  protocol          = "tcp"
  from_port         = 22
  to_port           = 22
  cidr_blocks       = ["${data.http.terraform_outbound_public_ip.body}/32"]
}

# Rule in SG for public subnet instances accessible from Internet,
# allowing ingress to SSH port from specified additional IPv4 CIDR blocks 
resource "aws_security_group_rule" "sub_pub_ingr_ssh_from_extras" {
  security_group_id = aws_security_group.sub_pub_ingr_spec_egr_any.id
  type              = "ingress"
  protocol          = "tcp"
  from_port         = 22
  to_port           = 22
  cidr_blocks       = var.extra_public_cidr_blocks_allowed_ssh_to_jmp
}

# Rule in SG for public subnet instances accessible from Internet,
# allowing egress to anywhere
resource "aws_security_group_rule" "sub_pub_egr_any" {
  security_group_id = aws_security_group.sub_pub_ingr_spec_egr_any.id
  type              = "egress"
  protocol          = "-1"
  from_port         = 0
  to_port           = 0
  cidr_blocks       = ["0.0.0.0/0"]
}

# Jump host instance in DMZ subnet 0
resource "aws_instance" "jmp" {
  ami                    = data.aws_ami.default.id
  instance_type          = var.instance_type
  subnet_id              = aws_subnet.dmz_0.id
  vpc_security_group_ids = [aws_security_group.sub_pub_ingr_spec_egr_any.id]
  key_name               = aws_key_pair.default.key_name

  tags = {
    name            = "jmp"
    role            = "jumphost"
    internet_access = "true"
  }
}

# Elastic IP address for jump host instance in DMZ subnet 0
resource "aws_eip" "i_jmp" {
  instance = aws_instance.jmp.id
}

# VPC's public subnets for load balancing
resource "aws_subnet" "pub" {
  count = 2

  vpc_id            = aws_vpc.main.id
  cidr_block        = "10.0.${1 + count.index}.0/24"
  availability_zone = random_shuffle.az_available.result[count.index]
}

# Route table association for DMZ subnet 0
resource "aws_route_table_association" "sub_pub" {
  count = 2

  subnet_id      = aws_subnet.pub[count.index].id
  route_table_id = aws_route_table.sub_pub.id
}


## Resources enabling Internet access through DMZ subnet 0
## (for private subnets)


# Elastic IP address for NAT gateway in DMZ subnet 0
resource "aws_eip" "nat_main" {}

# NAT gateway in DMZ subnet 0
resource "aws_nat_gateway" "main" {
  subnet_id     = aws_subnet.dmz_0.id
  allocation_id = aws_eip.nat_main.id
}


## Private subnets with Internet access


# Route table (RTb) for default route through NAT gateway, empty at first
# (for private subnets with Internet access)
resource "aws_route_table" "sub_priv_inet" {
  vpc_id = aws_vpc.main.id
}

# Post-creation route in Rtb for private subnets with Internet access:
# default route through NAT gateway
resource "aws_route" "default_gw_nat_main" {
  route_table_id         = aws_route_table.sub_priv_inet.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.main.id
}

# VPC's private subnets with Internet access
resource "aws_subnet" "priv_inet" {
  count = 2

  vpc_id            = aws_vpc.main.id
  cidr_block        = "10.0.${3 + count.index}.0/24"
  availability_zone = random_shuffle.az_available.result[count.index]
}

# Route table associations for private subnets with Internet access
resource "aws_route_table_association" "sub_priv_inet" {
  count = 2

  subnet_id      = aws_subnet.priv_inet[count.index].id
  route_table_id = aws_route_table.sub_priv_inet.id
}

# VPC's security group for instances in private subnets with Internet access,
# allowing at first:
#   - egress to anywhere
resource "aws_security_group" "sub_priv_inet" {
  vpc_id = aws_vpc.main.id

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Post-creation rule in SG for instances in private subnets with Internet
# access, allowing ingress to SSH port from private IP address of jump host
# in DMZ subnet 0
resource "aws_security_group_rule" "sub_priv_inet_ingr_ssh_from_jmp" {
  security_group_id = aws_security_group.sub_priv_inet.id
  type              = "ingress"
  protocol          = "tcp"
  from_port         = 22
  to_port           = 22
  cidr_blocks       = ["${aws_instance.jmp.private_ip}/32"]
}

# Post-creation rule in SG for instances in private subnets with Internet
# access, allowing ingress to application port from Application Load Balancer's
# SG
resource "aws_security_group_rule" "sub_priv_inet_ingr_lb_app" {
  security_group_id        = aws_security_group.sub_priv_inet.id
  type                     = "ingress"
  protocol                 = "tcp"
  from_port                = 8080
  to_port                  = 8080
  source_security_group_id = aws_security_group.lb_app.id
}

# Application host instances in DMZ subnets
resource "aws_instance" "app" {
  count = 2

  ami                    = data.aws_ami.default.id
  instance_type          = var.instance_type
  subnet_id              = aws_subnet.priv_inet[count.index].id
  vpc_security_group_ids = [aws_security_group.sub_priv_inet.id]
  key_name               = aws_key_pair.default.key_name
}


## Elastic Load Balancing - Application Load Balancer

# Security group
resource "aws_security_group" "lb_app" {
  vpc_id = aws_vpc.main.id
}

# Rule in SG for Application Load Balancer,
# allowing ingress to default HTTP port from Terraform host's outgoung public IP
# address
resource "aws_security_group_rule" "lb_app_ingr_http_from_terraform" {
  security_group_id = aws_security_group.lb_app.id
  type              = "ingress"
  protocol          = "tcp"
  from_port         = 80
  to_port           = 80
  cidr_blocks       = ["${data.http.terraform_outbound_public_ip.body}/32"]
}

# Rule in SG for Application Load Balancer,
# allowing ingress to default HTTP port from specified additional IPv4 CIDR
# blocks 
resource "aws_security_group_rule" "lb_app_ingr_http_from_extras" {
  security_group_id = aws_security_group.lb_app.id
  type              = "ingress"
  protocol          = "tcp"
  from_port         = 80
  to_port           = 80
  cidr_blocks       = var.extra_public_cidr_blocks_allowed_ssh_to_jmp
}

# Rule in SG for public subnet instances accessible from Internet,
# allowing egress to application port in DMZ subnets
resource "aws_security_group_rule" "lb_app_egr_priv_inet_app" {
  security_group_id        = aws_security_group.lb_app.id
  type                     = "egress"
  protocol                 = "tcp"
  from_port                = 8080
  to_port                  = 8080
  source_security_group_id = aws_security_group.sub_priv_inet.id
}

# Application Load Balancer
resource "aws_lb" "app" {
  subnets         = aws_subnet.pub[*].id
  security_groups = [aws_security_group.lb_app.id]
}

resource "aws_lb_target_group" "app" {
  port     = 8080
  protocol = "HTTP"
  vpc_id   = aws_vpc.main.id
}

resource "aws_lb_target_group_attachment" "app" {
  count = 2

  target_group_arn = aws_lb_target_group.app.arn
  target_id        = aws_instance.app[count.index].id
}

resource "aws_lb_listener" "app" {
  load_balancer_arn = aws_lb.app.arn
  port              = 80

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.app.id
  }
}


## Configuration of other tools


# OpenSSH client configuration file
resource "local_file" "ssh_config" {
  filename             = var.ssh_config
  directory_permission = "0700"
  file_permission      = "0600"
  content = templatefile(
    "templates/ssh/config.tpl",
    {
      default_user         = var.default_ami_user
      ssh_private_key_path = var.default_ssh_private_key_path
      jumphost = {
        alias    = "jmp"
        hostname = aws_eip.i_jmp.public_dns
      }
      hosts = [
        {
          alias    = "app0"
          hostname = aws_instance.app[0].private_dns
        },
        {
          alias    = "app1"
          hostname = aws_instance.app[1].private_dns
        },
      ]
    },
  )
}

# Ansible inventory (hosts) file
resource "local_file" "ansible_inventory" {
  filename = var.ansible_inventory
  content = templatefile(
    "templates/hosts.tpl",
    {
      hosts = [
        "jmp",
        "app0",
        "app1",
      ]
    },
  )
}

# Ansible `all` group vars file
resource "local_file" "ansible_group_vars_all" {
  filename = var.ansible_group_vars_all
  content = templatefile(
    "templates/group_vars/all/main.yml.tpl",
    {
      ansible_user    = var.default_ami_user
      lb_app_dns_name = aws_lb.app.dns_name
    },
  )
}

# Ansible configuration file
resource "local_file" "ansible_cfg" {
  depends_on = [
    local_file.ssh_config,
    local_file.ansible_inventory,
    local_file.ansible_group_vars_all,
  ]
  filename = "ansible.cfg"
  content = templatefile(
    "templates/ansible.cfg.tpl",
    {
      ansible_inventory = var.ansible_inventory
      ssh_config        = var.ssh_config
    },
  )
}

# Ansible vars file path for asserting Internet access state
resource "local_file" "ansible_vars_assert_internet_access_state" {
  filename = var.ansible_vars_assert_internet_access_state
  content = templatefile(
    "templates/vars/assert_internet_access_state.yml.tpl",
    {
      allowed_hosts = [
        "jmp",
        "app0",
        "app1",
      ]
    },
  )
}


## Instance connectivity tests


# SSH connectivity to jump host instance
resource "null_resource" "i_jmp_ssh_available" {
  triggers = {
    timestamp = timestamp()
  }
  depends_on = [aws_eip.i_jmp]

  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      host        = aws_eip.i_jmp.public_dns
      user        = var.default_ami_user
      private_key = file(var.default_ssh_private_key_path)
    }
  }
}

# SSH connectivity to application host instances behind jump host instance
resource "null_resource" "i_app_ssh_available" {
  count = 2

  triggers = {
    timestamp = timestamp()
  }
  depends_on = [
    null_resource.i_jmp_ssh_available,
    aws_instance.app,
  ]

  provisioner "remote-exec" {
    connection {
      type         = "ssh"
      bastion_host = aws_eip.i_jmp.public_dns
      host         = aws_instance.app[count.index].private_dns
      user         = var.default_ami_user
      private_key  = file(var.default_ssh_private_key_path)
    }
  }
}


## Further deployment commands


# Ansible system configuration playbook
resource "null_resource" "ansible_playbook" {
  triggers = {
    timestamp = timestamp()
  }
  depends_on = [
    local_file.ansible_cfg,
    local_file.ansible_vars_assert_internet_access_state,
    null_resource.i_app_ssh_available,
    aws_lb_listener.app,
  ]

  provisioner "local-exec" {
    command = "ansible-playbook ${var.ansible_playbook}"
  }
}
