---

allowed_hosts:%{ if allowed_hosts == [] } []
%{~ else }
%{ for host in allowed_hosts ~}
  - ${host}
%{ endfor ~}
%{ endif ~}
