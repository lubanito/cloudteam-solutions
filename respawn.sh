#!/bin/sh
set -o errexit
set -o nounset

$(dirname "$0")/destroy.sh -auto-approve
$(dirname "$0")/apply.sh -auto-approve
