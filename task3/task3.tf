##
## Terraform configuration
##


terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
    local = {
      source  = "hashicorp/local"
      version = "~> 2.0"
    }
    http = {
      source  = "hashicorp/http"
      version = "~> 2.0"
    }
    null = {
      source  = "hashicorp/null"
      version = "~> 3.0"
    }
  }
}


##
## Variables
##


# Default SSH public key file path
variable "default_ssh_public_key_path" {
  type    = string
  default = "~/.ssh/id_rsa.pub"
}

# Default SSH private key file path
variable "default_ssh_private_key_path" {
  type    = string
  default = ""
}

# Additional public IPv4 CIDR blocks allowed SSH access to jump host in DMZ
variable "extra_public_cidr_blocks_allowed_ssh_to_jmp" {
  type    = list(string)
  default = []
}

# Ansible playbook file path
# (playbook to be run, if available, on every Terraform apply
# once instances are created)
variable "ansible_playbook" {
  type    = string
  default = "task3.yml"
}

# Ansible vars file path for S3 application deployment
variable "ansible_vars_s3_app" {
  type    = string
  default = "vars/s3_app.yml"
}

# Ansible vars file path for asserting Internet access state
variable "ansible_vars_assert_internet_access_state" {
  type    = string
  default = "vars/assert_internet_access_state.yml"
}

# Ansible `all` group vars file path
variable "ansible_group_vars_all" {
  type    = string
  default = "group_vars/all/main.yml"
}

# Ansible inventory (hosts) generated file path
variable "ansible_inventory" {
  type    = string
  default = "hosts"
}

# OpenSSH client configuration generated file path
variable "ssh_config" {
  type    = string
  default = "ssh/config"
}

# Region
variable "region" {
  type    = string
  default = "us-east-1"
}

# Instance type
variable "instance_type" {
  type    = string
  default = "t2.micro" # free tier eligible, x86_64
}

# Name of default user account in default AMI
variable "default_ami_user" {
  type    = string
  default = "ec2-user"
}

# Public Internet HTTP service URL returning client's IPv4 address
# (outbound public address) as string
variable "outbound_public_ip_service_url" {
  type    = string
  default = "https://api.ipify.org/"
}


##
## Provider configuration
##


provider "aws" {
  region = var.region
}


##
## Data sources
##


# Amazon Machine Image:
# Latest non-beta RHEL 8 x86_64
data "aws_ami" "default" {
  most_recent = true
  owners      = [309956199498] # Red Hat

  filter {
    name   = "name"
    values = ["RHEL-8.*_HVM-*"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }
}

# SSH public key file
data "local_file" "default_ssh_public_key" {
  filename = pathexpand(var.default_ssh_public_key_path)
}

# Terraform host's outbound public IPv4 address
data "http" "terraform_outbound_public_ip" {
  url = var.outbound_public_ip_service_url
}


##
## Resources
##


## S3 bucket for application data, with folders


resource "aws_s3_bucket" "appdata" {
  tags = {
    app = "processing.sh"
  }
}

resource "aws_s3_bucket_object" "appdata_in" {
  bucket = aws_s3_bucket.appdata.id
  key    = "in/"
}

resource "aws_s3_bucket_object" "appdata_out" {
  bucket = aws_s3_bucket.appdata.id
  key    = "out/"
}


## IAM resources allowing access to S3 bucket from EC2 instances


# IAM role assumable by EC2 instances
resource "aws_iam_role" "ec2_allow_s3_full_access_to_bucket_appdata" {
  assume_role_policy = <<-EOF
    {
      "Version": "2012-10-17",
      "Statement": [
        {
          "Effect": "Allow",
          "Principal": {
            "Service": "ec2.amazonaws.com"
          },
          "Action": "sts:AssumeRole"
        }
      ]
    }
  EOF
}

# IAM policy defined inline in IAM role,
# allowing full access to contents of application data S3 bucket
resource "aws_iam_role_policy" "ec2_allow_s3_full_access_to_bucket_appdata" {
  role = aws_iam_role.ec2_allow_s3_full_access_to_bucket_appdata.id

  policy = <<-EOF
    {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Action": "s3:*",
                "Resource": [
                    "${aws_s3_bucket.appdata.arn}",
                    "${aws_s3_bucket.appdata.arn}/*"
                ]
            }
        ]
    }
  EOF
}

# IAM instance profile for passing IAM role to EC2 instances
resource "aws_iam_instance_profile" "allow_s3_full_access_to_bucket_data" {
  role = aws_iam_role.ec2_allow_s3_full_access_to_bucket_appdata.name
}


## Common


# SSH key pair / public key
resource "aws_key_pair" "default" {
  public_key = data.local_file.default_ssh_public_key.content
}

# VPC
resource "aws_vpc" "main" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true
}

# VPC's Internet gateway
resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id
}


## Public subnet (DMZ) with Internet access


# Route table (RTb) for public subnets, empty at first
resource "aws_route_table" "sub_pub" {
  vpc_id = aws_vpc.main.id

}

# Post-creation route in RTb for public subnets:
# default route through Internet gateway
resource "aws_route" "default_gw_igw_main" {
  route_table_id         = aws_route_table.sub_pub.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.main.id
}

# VPC's DMZ subnet 0
resource "aws_subnet" "dmz_0" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.0.0/24"
}

# Route table association for DMZ subnet 0
resource "aws_route_table_association" "sub_dmz_0" {
  subnet_id      = aws_subnet.dmz_0.id
  route_table_id = aws_route_table.sub_pub.id
}

# VPC's security group (SG) for public subnet instances having Internet access,
# but accessible only from specified public IPv4 address ranges
resource "aws_security_group" "sub_pub_ingr_spec_egr_any" {
  vpc_id = aws_vpc.main.id
}

# Rule in SG for public subnet instances accessible from Internet,
# allowing ingress to SSH port from Terraform host's outgoung public IP address
resource "aws_security_group_rule" "sub_pub_ingr_ssh_from_terraform" {
  security_group_id = aws_security_group.sub_pub_ingr_spec_egr_any.id
  type              = "ingress"
  protocol          = "tcp"
  from_port         = 22
  to_port           = 22
  cidr_blocks       = ["${data.http.terraform_outbound_public_ip.body}/32"]
}

# Rule in SG for public subnet instances accessible from Internet,
# allowing ingress to SSH port from specified additional IPv4 CIDR blocks 
resource "aws_security_group_rule" "sub_pub_ingr_ssh_from_extras" {
  security_group_id = aws_security_group.sub_pub_ingr_spec_egr_any.id
  type              = "ingress"
  protocol          = "tcp"
  from_port         = 22
  to_port           = 22
  cidr_blocks       = var.extra_public_cidr_blocks_allowed_ssh_to_jmp
}

# Rule in SG for public subnet instances accessible from Internet,
# allowing egress to anywhere
resource "aws_security_group_rule" "sub_pub_egr_any" {
  security_group_id = aws_security_group.sub_pub_ingr_spec_egr_any.id
  type              = "egress"
  protocol          = "-1"
  from_port         = 0
  to_port           = 0
  cidr_blocks       = ["0.0.0.0/0"]
}

# Jump host instance in DMZ subnet 0
resource "aws_instance" "jmp" {
  ami                    = data.aws_ami.default.id
  instance_type          = var.instance_type
  subnet_id              = aws_subnet.dmz_0.id
  vpc_security_group_ids = [aws_security_group.sub_pub_ingr_spec_egr_any.id]
  key_name               = aws_key_pair.default.key_name

  tags = {
    name            = "jmp"
    role            = "jumphost"
    internet_access = "true"
  }
}

# Elastic IP address for jump host instance in DMZ subnet 0
resource "aws_eip" "i_jmp" {
  instance = aws_instance.jmp.id
}


## Resources enabling Internet access through DMZ subnet 0
## (for private subnets)


# Elastic IP address for NAT gateway in DMZ subnet 0
resource "aws_eip" "nat_main" {}

# NAT gateway in DMZ subnet 0
resource "aws_nat_gateway" "main" {
  subnet_id     = aws_subnet.dmz_0.id
  allocation_id = aws_eip.nat_main.id
}


## Private subnet with Internet access


# Route table (RTb) for default route through NAT gateway, empty at first
# (for private subnets with Internet access)
resource "aws_route_table" "sub_priv_inet" {
  vpc_id = aws_vpc.main.id
}

# Post-creation route in Rtb for private subnets with Internet access:
# default route through NAT gateway
resource "aws_route" "default_gw_nat_main" {
  route_table_id         = aws_route_table.sub_priv_inet.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.main.id
}

# VPC's private subnet 0
resource "aws_subnet" "priv_0" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.1.0/24"
}

# Route table association for private subnet 0
resource "aws_route_table_association" "sub_priv_0" {
  subnet_id      = aws_subnet.priv_0.id
  route_table_id = aws_route_table.sub_priv_inet.id
}

# VPC's security group for instances in private subnets with Internet access,
# allowing at first:
#   - egress to anywhere
resource "aws_security_group" "sub_priv_inet" {
  vpc_id = aws_vpc.main.id

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Post-creation rule in SG for instances in private subnets with Internet
# access, allowing ingress to SSH port from private IP address of jump host
# in DMZ subnet 0
resource "aws_security_group_rule" "sub_priv_0_ingr_ssh_from_jmp" {
  security_group_id = aws_security_group.sub_priv_inet.id
  type              = "ingress"
  protocol          = "tcp"
  from_port         = 22
  to_port           = 22
  cidr_blocks       = ["${aws_instance.jmp.private_ip}/32"]
}

# Development host instance in private subnet 0,
# allowed access to S3 bucket using IAM instance profile
resource "aws_instance" "dev0" {
  ami                    = data.aws_ami.default.id
  instance_type          = var.instance_type
  subnet_id              = aws_subnet.priv_0.id
  vpc_security_group_ids = [aws_security_group.sub_priv_inet.id]
  key_name               = aws_key_pair.default.key_name
  iam_instance_profile   = aws_iam_instance_profile.allow_s3_full_access_to_bucket_data.name

  tags = {
    name            = "dev0"
    internet_access = "true"
  }
}


## Configuration of other tools


# OpenSSH client configuration file
resource "local_file" "ssh_config" {
  filename             = var.ssh_config
  directory_permission = "0700"
  file_permission      = "0600"
  content = templatefile(
    "templates/ssh/config.tpl",
    {
      default_user         = var.default_ami_user
      ssh_private_key_path = var.default_ssh_private_key_path
      jumphost = {
        alias    = "jmp"
        hostname = aws_eip.i_jmp.public_dns
      },
      hosts = [
        {
          alias    = "dev0"
          hostname = aws_instance.dev0.private_dns
        },
      ]
    },
  )
}

# Ansible inventory (hosts) file
resource "local_file" "ansible_inventory" {
  filename = var.ansible_inventory
  content = templatefile(
    "templates/hosts.tpl",
    {
      hosts = [
        "jmp",
        "dev0",
      ]
    },
  )
}

# Ansible `all` group vars file
resource "local_file" "ansible_group_vars_all" {
  filename = var.ansible_group_vars_all
  content  = <<-EOF
    ---

    ansible_user: ${var.default_ami_user}
    EOF
}

# Ansible configuration file
resource "local_file" "ansible_cfg" {
  depends_on = [
    local_file.ssh_config,
    local_file.ansible_inventory,
  ]
  filename = "ansible.cfg"
  content = templatefile(
    "templates/ansible.cfg.tpl",
    {
      ansible_inventory = var.ansible_inventory
      ssh_config        = var.ssh_config
    },
  )
}

# Ansible vars file for asserting Internet access state
resource "local_file" "ansible_vars_assert_internet_access_state" {
  filename = var.ansible_vars_assert_internet_access_state
  content = templatefile(
    "templates/vars/assert_internet_access_state.yml.tpl",
    {
      allowed_hosts = [
        "jmp",
        "dev0",
      ]
    },
  )
}

# Ansible vars file for S3 application deployment
resource "local_file" "ansible_vars_s3_app" {
  filename = var.ansible_vars_s3_app
  content = templatefile(
    "templates/vars/s3_app.yml.tpl",
    {
      appdata_s3_bucket_name = aws_s3_bucket.appdata.id
    },
  )
}


## Instance connectivity tests


# SSH connectivity to jump host instance
resource "null_resource" "i_jmp_ssh_available" {
  triggers = {
    timestamp = timestamp()
  }
  depends_on = [aws_instance.jmp]

  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      host        = aws_eip.i_jmp.public_dns
      user        = var.default_ami_user
      private_key = file(var.default_ssh_private_key_path)
    }
  }
}

# SSH connectivity to host instances behind jump host instance

resource "null_resource" "i_dev0_ssh_available" {
  triggers = {
    timestamp = timestamp()
  }
  depends_on = [
    null_resource.i_jmp_ssh_available,
    aws_instance.dev0,
  ]

  provisioner "remote-exec" {
    connection {
      type         = "ssh"
      bastion_host = aws_eip.i_jmp.public_dns
      host         = aws_instance.dev0.private_dns
      user         = var.default_ami_user
      private_key  = file(var.default_ssh_private_key_path)
    }
  }
}


## Further deployment commands


# Ansible system configuration playbook
resource "null_resource" "ansible_playbook" {
  triggers = {
    timestamp = timestamp()
  }
  depends_on = [
    local_file.ansible_cfg,
    local_file.ansible_vars_assert_internet_access_state,
    null_resource.i_jmp_ssh_available,
    null_resource.i_dev0_ssh_available,
  ]

  provisioner "local-exec" {
    command = "ansible-playbook ${var.ansible_playbook}"
  }
}
